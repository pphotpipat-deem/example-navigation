//
// Created by Paul Photpipat on 18/8/2022 AD.
//

import SwiftUI
import Combine

internal struct RideHailSearchView: View {

    var body: some View {
        VStack {
            Text("Ride Hail Search Form")
            NavigationLink(destination: RideHailPickUpDropOffView().navigationBarHidden(true)) {
                Text("Pick-up and Drop-off")
            }
            NavigationLink(destination: RideHailSearchResultView().navigationBarHidden(true)) {
                Text("SEARCH")
                        .bold()
                        .foregroundColor(.red)
            }
        }
    }
}
