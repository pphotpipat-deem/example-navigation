//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import SwiftUI

internal struct RideHailPickUpDropOffView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack {
            Text("Ride Hail Pick-up and Drop-Off View")
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Text("<- Back")
            }
        }
    }
}

