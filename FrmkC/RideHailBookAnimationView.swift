//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import SwiftUI

internal struct RideHailBookAnimationView: View {
    @State var timeRemaining = 3
    
    @EnvironmentObject private var viewRouter: ViewRouter
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    var body: some View {
        VStack {
            Text("Ride Hail Book Animation view")
            Text("\(timeRemaining)")
                .onReceive(timer) { input in
                    if timeRemaining > 0 {
                        timeRemaining -= 1
                    } else {
                        viewRouter.currentView = .confirmBooking
                    }
                }
            NavigationLink(
                destination: RideHailBookConfirmView().navigationBarHidden(true),
                tag: MobilityView.confirmBooking,
                selection: $viewRouter.currentView
            ) {
                EmptyView()
            }
        }
    }
}
