//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import UIKit
import SwiftUI
import Combine

public final class RideHailSearchScreenFactory {
    private init() {}

    public static func makeUIKit(toMain: @escaping () -> Void) -> UIViewController {
        let rideHailSearchView = BookRideHailFlow(toMain: toMain)
        let hostingController = UIHostingController(rootView: rideHailSearchView)
        return hostingController
    }
}
