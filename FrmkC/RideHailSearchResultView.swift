//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import SwiftUI

internal struct RideHailSearchResultView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack {
            Text("Search Result View")
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Text("<- Back")
            }
            NavigationLink(destination: RideHailConfirmLocationView().navigationBarHidden(true)) {
                Text("Select result to book -> ")
                        .bold()
                        .foregroundColor(.green)
            }
        }
    }
}
