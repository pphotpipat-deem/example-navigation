//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import SwiftUI

internal struct RideHailBookConfirmView: View {
    
    @EnvironmentObject private var viewRouter: ViewRouter

    var body: some View {
        VStack {
            Text("Ride Hail Book Confirm view")
            Button(action: {
                viewRouter.currentView = .root
            }) {
                Text("DONE")
            }
        }
    }
}
