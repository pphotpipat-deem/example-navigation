//
// Created by Paul Photpipat on 22/8/2022 AD.
//

import SwiftUI

internal struct RideHailConfirmLocationView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State var isTermAndConditionPreseting = false
    
    var body: some View {
        VStack {
            Text("Ride Hail Confirm location view")
            Button(action: {isTermAndConditionPreseting.toggle()}) {
                Text("Term and Condition")
            }.sheet(isPresented: $isTermAndConditionPreseting) {
                RideHailTermAndConditionView()
            }
            
            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Text("<- Back")
            }
            NavigationLink(destination: RideHailBookAnimationView().navigationBarHidden(true)) {
                Text("Book... ")
                        .bold()
                        .foregroundColor(.purple)
            }
        }
    }
}
