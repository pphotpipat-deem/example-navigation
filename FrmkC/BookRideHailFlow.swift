import SwiftUI

enum MobilityView {
    case root, searchForm, searchResult, confirmPickup, bookAnimation, confirmBooking
}

class ViewRouter: ObservableObject {
    @Published var currentView: MobilityView? = nil
}

struct BookRideHailFlow: View {
    
    let toMain: () -> Void
    
    @StateObject private var viewRouter = ViewRouter()
    
    var body: some View {
        NavigationView {
            RideHailSearchView().navigationBarHidden(true)
        }
        .environmentObject(viewRouter)
        .navigationBarHidden(true)
        .onReceive(viewRouter.$currentView) { currentView in
            if currentView == .root {
                toMain()
            }
        }
    }
}
