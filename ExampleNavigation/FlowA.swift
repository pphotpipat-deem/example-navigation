import Core
import FrmkA
import FrmkB
import FrmkC
import UIKit

final class FlowA {

    private let navigationController: UINavigationController
    private lazy var innerNavigationController = UINavigationController()
    private lazy var urlSession: URLSession = URLSession.shared

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let apolloRemote = ApolloClientRemote(urlSession: urlSession)
        let loader = UserLoaderRemote(apolloClient: apolloRemote, mapper: { apolloResponse in
            return User(username: apolloResponse.username)
        })
        let loginViewController = LoginScreenFactory.make(
            onLoginSuccess: makeMain(),
            loader: loader
        )
        navigationController.pushViewController(loginViewController, animated: false)
    }

    private func makeMain() -> (String) -> Void {
        { [weak self] user in
            guard let self = self else { return }
            let mainViewController = MainScreenFactory.make(user: user,
                    tapped: self.makeDetail(),
                    rideHailTapped: self.makeRideHailSearch())
            self.innerNavigationController.setViewControllers([mainViewController], animated: false)
            self.navigationController.present(self.innerNavigationController, animated: true)
        }
    }

    private func makeDetail() -> () -> Void {
        { [weak self] in
            guard let self = self else { return }
            let detailViewController = DetailScreenFactory.makeUIKit(onSelectOption: self.makeOptionsView())
            self.innerNavigationController.pushViewController(detailViewController, animated: true)
        }
    }

    private func makeRideHailSearch() -> () -> Void {
        { [weak self] in
            guard let self = self else {
                return
            }
            let rideHailSearchViewController = RideHailSearchScreenFactory.makeUIKit(toMain: {
                self.navigationController.popViewController(animated: true)
            })
            self.innerNavigationController.dismiss(animated: true)
            self.navigationController.pushViewController(rideHailSearchViewController, animated: true)
        }
    }

    private func makeOptionsView() -> (DetailViewCallback) -> Void {
        { [weak self] callback in
            guard let self = self else { return }
            let optionsView = OptionSelectionScreenFactory.makeUIKit(
                options: ["A", "B", "C"].map { .init(description: $0) },
                onSelection: { option in
                    callback.didSelectOption(option)
                    self.innerNavigationController.dismiss(animated: true)
                })
            optionsView.title = "Select an option"
            self.innerNavigationController.present(UINavigationController(rootViewController: optionsView), animated: true)
        }
    }
}
