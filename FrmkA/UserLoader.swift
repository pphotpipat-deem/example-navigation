import Core
import Foundation

public protocol UserLoader {
    func fetchUser(
        username: String,
        password: String,
        completion: (Result<User, Error>) -> Void
    )
}

public class UserLoaderRemote: UserLoader {
    private let apolloClient: ApolloClient
    private let mapper: (LoginResult) -> User

    public init(
        apolloClient: ApolloClient,
        mapper: @escaping (LoginResult) -> User
    ) {
        self.apolloClient = apolloClient
        self.mapper = mapper
    }

    public func fetchUser(username: String, password: String, completion: (Result<User, Error>) -> Void) {
        let query = LoginQuery(username: username, password: password)
        let result: LoginResult = apolloClient.query(query)
        let user = mapper(result)
        completion(.success(user))
    }
}
