//
//  LoginUseCase.swift
//  FrmkA
//
//  Created by Fabio Mignogna on 10/08/2022.
//

import Foundation

class LoginUseCase {
    private let loader: UserLoader

    init(loader: UserLoader) {
        self.loader = loader
    }

    func login(username: String, password: String, completion: (Result<String, Error>) -> Void) {
        loader.fetchUser(username: username, password: password) { result in
            switch  result {
            case let .success(user):
                completion(.success(user.username))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
