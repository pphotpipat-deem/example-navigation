import UIKit

internal final class LoginViewController: UIViewController {

    private var onLoginSuccess: ((String) -> Void)?
    private var viewModel: LoginViewModel?

    private lazy var loginButton: UIButton = {
        let btn = UIButton(type: .roundedRect)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Login", for: .normal)
        btn.titleLabel?.font = .preferredFont(forTextStyle: .largeTitle)
        btn.addTarget(self, action: #selector(loginButtonOnTap), for: .touchUpInside)
        return btn
    }()

    convenience init(onLoginSuccess: @escaping (String) -> Void, viewModel: LoginViewModel) {
        self.init()
        self.onLoginSuccess = onLoginSuccess
        self.viewModel = viewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()

        viewModel?.onSuccess = { [weak self] username in
            self?.onLoginSuccess?(username)
        }

        viewModel?.onFailure = { print("error") }
    }

    private func setupUI() {
        title = "Login"
        view.backgroundColor = .white
        view.addSubview(loginButton)

        NSLayoutConstraint.activate([
            loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loginButton.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    @objc private func loginButtonOnTap() {
        viewModel?.login(username: "foo", password: "bar")
    }
}
