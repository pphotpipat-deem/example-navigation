public struct User {
    public let username: String

    public init(username: String) {
        self.username = username
    }
}
