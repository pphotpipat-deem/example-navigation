import Foundation

public class ApolloClientRemote: ApolloClient {
    private let urlSession: URLSession

    public init(urlSession: URLSession) {
        self.urlSession = urlSession
    }

    public func query<Q, R>(_ q: Q) -> R where Q : ApolloQuery, R : ApolloResponse {
        //get some data from network
        let data = Data("fabio".utf8)
        return R(data: data)
    }
}
