import Foundation

public protocol ApolloQuery {
    associatedtype Response: ApolloResponse
}
public protocol ApolloResponse {
    init(data: Data)
}

public protocol ApolloClient {
    func query<Q: ApolloQuery, R: ApolloResponse>(_ q: Q) -> R
}


