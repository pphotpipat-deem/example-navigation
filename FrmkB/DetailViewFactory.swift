import UIKit
import SwiftUI

public final class DetailScreenFactory {
    private init() {}

    public static func makeUIKit(onSelectOption: @escaping (DetailViewCallback) -> Void) -> UIViewController {
        let detailView = DetailView(selectionOption: onSelectOption)
        let hostingController = UIHostingController(rootView: detailView)
        return hostingController
    }
}
