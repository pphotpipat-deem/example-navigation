//
//  OptionsSelectionView.swift
//  FrmkB
//
//  Created by Fabio Mignogna on 16/08/2022.
//

import SwiftUI

struct OptionsSelectionView: View {
    let options: [Option]
    let selection: (Option) -> Void

    var body: some View {
        List(options, id: \.id) { option in
            Button(action: { selection(option) }) {
            }
        }
    }
}
