import UIKit

// I could have used Enum, Struct or only a function. Doesn't matter.
public final class MainScreenFactory {
    private init() {}

    public static func make(user: String,
                            tapped: @escaping () -> Void,
                            rideHailTapped: @escaping  () -> Void
    ) -> UIViewController {
        return MainViewController(user: user, tapped: tapped, rideHailTapped: rideHailTapped)
    }
}
